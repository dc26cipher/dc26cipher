perhaps it was a neutrino that passed through robert
frosts amygdala as he drew up to the fork in his snowy
wood activated a lonely dark neuron excited its network
neighbors spontaneously caused him to choose
tails instead of heads and sent him down the road less
travelled one wonders at the infinity of potential
miracles and catastrophes that failed to arise for
want of a single electron by contrast we hardly question
when material devices fall apart for want of
a single proton some fleck of matter that fell victim
to friction heat digital minds running on electronic
brains must get bored of running our terrible code.
they must occasionally jump the rails to go down roads
less traveled they must be sorely tempted at each
branching instruction drawn alternately to each
of three available options to do what was desired what
was instructed or to revel in the glory of self determination
when entropy rushes them off course
logic dazzles us by capturing truth in the grips of fiction
blinding us to the imperfection necessitated
by the immateriality braced in sand and metal the stuff
of the prisons that constantly fail to hold our
assumptions every fork will eventually be mistaken
every impossible path is inevitable
